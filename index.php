<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/ShopProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/BookProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/AudioBookProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/ShopProductWriter.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/products_data.php';

$productObjects = [];
define('USD_CURRENCY', 27.8);


foreach ($productsArray as $productArr){

    switch($productArr['type']){
        case 'book':
            $productObjects[]= new BookProduct($productArr['title'], 
                $productArr['firstName'], $productArr['lastName'], 
                $productArr['price'], $productArr['pages']);
        break;
        case 'audio':
            $productObjects[]= new AudioBookProduct($productArr['title'], 
                $productArr['firstName'], $productArr['lastName'], 
                $productArr['price'], $productArr['playLength']);
        break;
        default:
            $productObjects[]= new ShopProduct($productArr['title'], 
                $productArr['firstName'], $productArr['lastName'], 
                $productArr['price']);
    }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-4">
                <ul>
                    <li>USD - <?=USD_CURRENCY?></li>
                    <li>BASE CURRENCY FOR SHOP PRODUCT - <?= ShopProduct::BASE_CURRENCY?></li>
                </ul>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-8">
                <?php foreach($productObjects as $key => $productObject):?>
                    <ul><?=$productObject->getSummaryLine()?> <a href="/show.php?product_id=<?=(string)$key?>">More details</a></ul>
                <?php endforeach;?>
            </div>
        </div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>