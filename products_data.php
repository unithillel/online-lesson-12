<?php
$productsArray = [
    [
        'title' => 'PHP is awesome',
        'firstName' => 'John',
        'lastName' => 'Doe',
        'price' => 30,
        'pages' => 250,
        'type' => 'book'
    ],
    [
        'title' => 'Javascript is awesome',
        'firstName' => 'Bob',
        'lastName' => 'Robbinson',
        'price' => 25,
        'playLength' => 180,
        'type' => 'audio'
        
    ],
    [
        'title' => 'Html is awesome',
        'firstName' => 'Allice',
        'lastName' => 'Grace',
        'price' => 14,
        'pages' => 250,
        'type' => 'book'
    ],
    [
        'title' => 'CSS is awesome',
        'firstName' => 'Ann',
        'lastName' => 'Johnson',
        'price' => 27,
        'playLength' => 170,
        'type' => 'audio'
    ]
];