<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/ShopProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/BookProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/AudioBookProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/ShopProductWriter.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/BookProductWriter.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/AudioBookProductWriter.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/products_data.php';

if(!empty($_GET['product_id']) || $_GET['product_id'] == 0){
    $productId = $_GET['product_id'];
}else{
    header('Location:/');
    die();
}

$productArr = $productsArray[$productId];
$writer = new ShopProductWriter();
switch($productArr['type']){
    case 'book':
        $productObject = new BookProduct($productArr['title'], 
            $productArr['firstName'], $productArr['lastName'], 
            $productArr['price'], $productArr['pages']);
        $writer = new BookProductWriter();
    break;
    case 'audio':
        $productObject = new AudioBookProduct($productArr['title'], 
            $productArr['firstName'], $productArr['lastName'], 
            $productArr['price'], $productArr['playLength']);
        $writer = new AudioBookProductWriter();
    break;
    default:
        $productObject = new ShopProduct($productArr['title'], 
            $productArr['firstName'], $productArr['lastName'], 
            $productArr['price']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?=$writer->write($productObject)?>
</body>
</html>