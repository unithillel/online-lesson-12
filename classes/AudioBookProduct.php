<?php
class AudioBookProduct extends ShopProduct{
    
    public $playLength = 0;

    public function __construct($title, $firstName, $lastName, $price, $playLength){
        parent::__construct($title, $firstName, $lastName, $price);
        $this->playLength = $playLength;
    }

    public function getSummaryLine(){
        return parent::getSummaryLine()  . ' play length:' . $this->playLength;
    }
}